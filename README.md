# IoT Project 2: Processing Images

C++ program that uses OpenCV to perform a number of image processing techniques on remotly fetched images.

![Example program output](./assets/example.jpg)

## Requirements

Requires `g++`, `cmake`, `vcpkg`.

## Prerequisites

### restclient-cpp

Install [restclient-cpp](https://github.com/mrtazz/restclient-cpp) using `vcpkg`.

```bash
sudo -E vcpkg integrate install
sudo -E vcpkg install restclient-cpp
```

### nlohmann-json

Install [nlohmann-json](https://github.com/nlohmann/json) using `vcpkg`.

```bash
sudo -E vcpkg integrate install
sudo -E vcpkg install nlohmann-json
```

## Build & run

Build the main application using `CMake` and run it using the following command:

```bash
./build/main
```

## Usage

Images are fetched using `wget` and [aws.random.cat/meow](https://aws.random.cat/meow). The program works only with `png`, `jpg`, and `jpeg` images. The program will attempt to fetch a new image if the received image is not of a compatible type. The API endpoint, number of attempts, and retry interval can be adjusted in the program.

### Naming

Processed images retain their original names. In situations where blending is used, the resulting image will retain the name of the first of the two blended images.

### Data

Downloaded images in their original form are stored in the [/data/original](./data/original) directory. Processed images are store in the [/data/output](./data/output) directory.

### Examples

| Source 1        | Source 2                           | Result                          |
| ---             | ---                                | ---                             |
| [9f4dc28d4817a0d8217198b46846d3fb.jpg](./data/original/9f4dc28d4817a0d8217198b46846d3fb.jpg) |  [dsc01990.jpg](./data/original/dsc01990.jpg)  | [9f4dc28d4817a0d8217198b46846d3fb.jpg](./data/output/9f4dc28d4817a0d8217198b46846d3fb.jpg) |
| [lbXht.jpg](./data/original/lbXht.jpg) |  [iqROl.jpg](./data/original/iqROl.jpg)  | [lbXht.jpg](./data/output/lbXht.jpg) |
| [tumblr_lnbhr9gsbC1qg6j9eo1_500.jpg](./data/original/tumblr_lnbhr9gsbC1qg6j9eo1_500.jpg) |  [Geqgs.jpg](./data/original/Geqgs.jpg)  | [tumblr_lnbhr9gsbC1qg6j9eo1_500.jpg](./data/output/tumblr_lnbhr9gsbC1qg6j9eo1_500.jpg) |
| [nXneX.jpg](./data/original/nXneX.jpg) |  [fergus_05.jpg](./data/original/fergus_05.jpg)  | [nXneX.jpg](./data/output/nXneX.jpg) |
| [9qd2Z.jpg](./data/original/9qd2Z.jpg) |  [4VewR.jpg](./data/original/4VewR.jpg)  | [9qd2Z.jpg](./data/output/9qd2Z.jpg) |

## Avaliable techniques

The available effects are shown in the following table:

| Effect        | Description                                                                               |
| ---           | ---                                                                                       |
| [Add border](#add-border)    | Adds a black border to an image. Border size is configurable.                             |
| [Add text](#add-text)      | Adds text to an image. Text is configurable.                                              |
| [Add threshold](#add-threshold) | Adds threshold to an image. Threshold value and max value are configurable.               |
| [Add blur](#add-blur)      | Adds blur to an image. The amount of blur is configurable.                                |
| [Balance tone](#balance-tone)  | Balances tone of an image.                                                                |
| [Blend images](#blend-images)  | Blends two images if they contain the same number of channels e.g. color, black & white.  |
| [Set size](#set-size)      | Sets the max width and max height of an image. Max width and max height are configurable. |

### Add border

Adds a black border to an image. Border size is configurable.

Before:

![Before adding border](./assets/border/border-before.jpg)

After:

![After adding border](./assets/border/border-after.jpg)

### Add text

Adds text to an image. Text is configurable.

Before:

![Before adding text](./assets/text/text-before.jpg)

After:

![After adding text](./assets/text/text-after.jpg)

### Add threshold

Adds threshold to an image. Threshold value and max value are configurable.

Before:

![Before adding threshold](./assets/threshold/threshold-before.jpg)

After:

![After adding threshold](./assets/threshold/threshold-after.jpg)

### Add blur

Adds blur to an image. The amount of blur is configurable.

Before:

![Before adding blur](./assets/blur/blur-before.jpg)

After:

![After adding blur](./assets/blur/blur_after.jpg)

### Balance tone

Balances tone of an image.

Before:

![Before balancing tone](./assets/balance/balance-before.jpg)

After:

![After balancing tone](./assets/balance/balance-after.jpg)

### Blend images

Blends two images if they contain the same number of channels e.g. color, black & white.

Source image 1:

![Source image 1](./assets/blend/blend-before-1.jpg)

Source image 2:

![Source image 2](./assets/blend/blend-before-2.jpg)

After:

![After blending images 1 and 2](./assets/blend/blend-after.jpg)

### Set size

Sets the max width and max height of an image. Max width and max height are configurable.

Before:

![Before setting size](./assets/size/size-before.jpg)

After:

![After setting size](./assets/size/size-after.jpg)


## Maintainers

Nizar ([@nizos](https://gitlab.com/nizos)) & Miriam ([@MiriamL](https://gitlab.com/MiriamL))

## Contributions

[@MiriamL](https://gitlab.com/MiriamL):

* [Get remote image](https://gitlab.com/nizos/assignment-5/-/issues/1)
* [Save image](https://gitlab.com/nizos/assignment-5/-/issues/2)
* [Add text](https://gitlab.com/nizos/assignment-5/-/issues/5)
* [Add border](https://gitlab.com/nizos/assignment-5/-/issues/6)
* [Add blur](https://gitlab.com/nizos/assignment-5/-/issues/8)
* [Balance tone](https://gitlab.com/nizos/assignment-5/-/issues/9)

[@nizos](https://gitlab.com/nizos):

* [Read image](https://gitlab.com/nizos/assignment-5/-/issues/3)
* [Set size](https://gitlab.com/nizos/assignment-5/-/issues/4)
* [Add threshold](https://gitlab.com/nizos/assignment-5/-/issues/7)
* [Blend images](https://gitlab.com/nizos/assignment-5/-/issues/10)
* [Set size add height constraint](https://gitlab.com/nizos/assignment-5/-/issues/11)
* [Check if image is of compatible extension](https://gitlab.com/nizos/assignment-5/-/issues/12)

## License

GPL-3.0 License
