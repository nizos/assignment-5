#include <iostream>
#include <vector>
#include <string>
#include <stdio.h>
#include <algorithm>
#include <chrono>
#include <thread>
#include <nlohmann/json.hpp>
#include <opencv2/opencv.hpp>
#include "restclient-cpp/restclient.h"

using json = nlohmann::json;

// API
std::string getRemoteImage(std::string url);

// Storage
cv::Mat readImage(std::string name);
void saveImage(cv::Mat image, std::string name);

// Effects
void addBorder(cv::Mat &image, int size);
void addText(cv::Mat &image, std::string text);
void addThreshold(cv::Mat &image, int value, int max);
void addBlur(cv::Mat &image, int size);
void balanceTone(cv::Mat &image);
void blendImages(cv::Mat &src1, cv::Mat &src2, double alpha);

// Utilities
void setSize(cv::Mat &image, int max_width, int max_height);

// Helpers
std::string getFileName(std::string path);
std::string getFileExtension(std::string path);
std::string stripDoubleQuotes(std::string src);
bool isCompatibleExtension(std::string extension);

// Constants
const int MAX_ATTEMPTS = 20; 
const int RETRY_TIMOUT_MS = 2000;
const std::string API = "https://aws.random.cat/meow";
const std::string SRC_PATH = "./data/original/";
const std::string DST_PATH = "./data/output/";
const std::string ALLOWED_EXTS[3] = { "png", "jpg", "jpeg"};

int main()
{
    // Get remote image
    std::string name = getRemoteImage(API);

    // Read image
    cv::Mat image = readImage(name);

    // Set size: max width 300px, max height 300px, preserve aspect ratio
    setSize(image, 300, 300);

    // Blend images
    std::string name2 = getRemoteImage(API);
    cv::Mat image2 = readImage(name2);
    blendImages(image, image2, 0.5);

    // Balance tone
    balanceTone(image);

    // Add threshold
    addThreshold(image, 127, 255);

    // Add blur
    addBlur(image, 10);

    // Add text
    addText(image, "Nizar & Miriam");

    // Add border, size of border recommended between 1-10
    addBorder(image, 5);

    // Save image
    saveImage(image, name);

    return 0;
}

// Fetches remote image from API and stores it
std::string getRemoteImage(std::string url)
{
    // Get image file address
    std::string path = "";
    int attempt = MAX_ATTEMPTS;
    while (attempt > 0)
    {
        // Parse image address from API response
        auto body = json::parse(RestClient::get(url).body);
        path = body["file"].dump();

        // If file extension is not of compatible type
        path = stripDoubleQuotes(path);
        if (!isCompatibleExtension(getFileExtension(path)))
        {
            // Retry
            std::this_thread::sleep_for(std::chrono::milliseconds(RETRY_TIMOUT_MS));
            attempt--;
        }
        else
        {
            break;
        }
    }

    // Get and store remote image
    std::string command = "wget " + path + " -P " + SRC_PATH;
    system(command.c_str());

    // Parse and return file name from address
    return getFileName(path);
}

// Strips double quote from a string and returns it
std::string stripDoubleQuotes(std::string src)
{
    src.erase(std::remove(src.begin(), src.end(), '\"' ), src.end());
    return src;
}

// Returns the file name from file path string
std::string getFileName(std::string path)
{
    size_t index = path.find_last_of("/\\");
    return path.substr(index+1);
}

// Returns the file extension from file path string
std::string getFileExtension(std::string path)
{
    size_t index = path.find_last_of(".");
    return path.substr(index+1);
}

// Checks whether provided extension is of compatible type
bool isCompatibleExtension(std::string extension)
{
    // If extension is in allowed extensions array
    if (std::find(std::begin(ALLOWED_EXTS), std::end(ALLOWED_EXTS), extension) != std::end(ALLOWED_EXTS))
    {
        return true;
    }
    return false;
}

// Stores an image Mat to file system
void saveImage(cv::Mat image, std::string name)
{
    cv::imwrite(DST_PATH + name, image);
}

// Loads image from file system as Mat
cv::Mat readImage(std::string name)
{
    return cv::imread(SRC_PATH + name);
}

// Adds text to an image
void addText(cv::Mat &image, std::string text)
{
    cv::putText(image, text, cv::Point(50, 50), cv::FONT_HERSHEY_DUPLEX, 1.0, cv::Scalar(0, 255, 0), 1, cv::LINE_AA, false);
}

// Adds border to an image
void addBorder(cv::Mat &image, int size)
{
    int ver = 0.01*size*image.rows;
    int hor = 0.01*size*image.cols;
    copyMakeBorder(image, image, ver, ver, hor, hor, cv::BORDER_CONSTANT);
}

// Adds threshold effect to an image
void addThreshold(cv::Mat &image, int value, int max)
{
    // Convert to gray
    cv::cvtColor(image, image, cv::COLOR_BGR2GRAY);

    // Add threshold
    cv::threshold(image, image, value, max, cv::THRESH_BINARY);
}

// Resizes an image to given max width and max height
void setSize(cv::Mat &image, int max_width, int max_height)
{
    // Calculate width and height scaling for provided dimensions
    double scale_width = float(max_width)/image.size().width;
    double scale_height = float(max_height)/image.size().height;

    // Scale according to the larger of the two dimensions
    double scale = (scale_width < scale_height) ? scale_width : scale_height;
    cv::resize(image, image, cv::Size(), scale, scale, cv::INTER_LINEAR);
}

// Adds blur effect to an image
void addBlur(cv::Mat &image, int size)
{
    cv::blur(image, image, cv::Size(size, size)); 
}

// Adds tone balance effect to an image
void balanceTone(cv::Mat &image)
{
    // Convert image from BGR to YCrCb
    cv::cvtColor(image, image, cv::COLOR_BGR2YCrCb);

    // Separate channels: Y, Cr, and Cb
    std::vector<cv::Mat> channels;
    cv::split(image, channels);

    // Equalize Y channel
    cv::equalizeHist(channels[0], channels[0]);

    // Merge channels
    cv::merge(channels, image);

    // Convert image from YCrCb to BGR
    cv::cvtColor(image, image, cv::COLOR_YCrCb2BGR);
}

// Blend two images with alpha control
void blendImages(cv::Mat &src1, cv::Mat &src2, double alpha)
{
    try
    {
        // Resize image 2 to match size of image 1
        cv::resize(src2, src2, cv::Size(src1.size().width, src1.size().height), cv::INTER_LINEAR);

        // Blend images 1 and 2
        cv::addWeighted(src1, alpha, src2, ( 1.0 - alpha ), 0.0, src1);
    }
    catch (cv::Exception & ex)
    {
        // Image 1 & 2 have different channel counts e.g. rgb & bw.
        std::cout << "Error: The provided images have different number of channels!" << std::endl;
        std::cout << "Exception: " << ex.what() << std::endl;
    }
}
